import re
from tkinter import *
  
root = Tk()
scrollbar = Scrollbar(root)
scrollbar.pack( side = RIGHT, fill = Y )

mylist = Listbox(root, width = 700, yscrollcommand = scrollbar.set)

srcFolder = "edited/"
busLineNumber = 223
text = ""

# Read file
rf = open(srcFolder + str(busLineNumber) +".txt", "r", encoding="utf8")

for line in rf:
	mylist.insert(END, line)

rf.close()

mylist.pack( side = LEFT, fill = BOTH )
scrollbar.config( command = mylist.yview )

# root window title and dimension
root.title(str(busLineNumber))

# Set geometry (widthxheight)
root.geometry('700x700')
  
root.mainloop()