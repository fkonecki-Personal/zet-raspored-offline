# ZET Raspored Offline
Projekt sadrži skripte za (većinom) automatizirano prikupljanje informacija iz ZET-ovih rasporeda.
Cilj je jednostavna aplikacija s rasporedima svih autobusnih linija spremljenih unutar aplikacije (u file-ovima ili DB) za offline korištenje

# Sadržaj glavne datoteke
## Linkovi.txt
Sadrži linkove na rasporede svih autobusnih linija.

*Linkovi su prikupljeni na sljedeći način:
Na stranici https://hackertarget.com/extract-links/ (online alata za dohvaćanje svih linkova na nekoj web stranici) je unesen link na ZET-ov popis dnevnih autobusnih linija https://www.zet.hr/autobusni-prijevoz/dnevne-linije-251/251
Popis koji vrati stranica je kopiran u linkovi.txt i uklonjeni su linkovi koji ne sadrže rasporede.

## Skripta downloader.py
Ova skripta čita linkove iz linkovi.txt, preuzima pdf-ove s linkova i sprema ih u datoteku pdf.

## Datoteke pdf i pdf2
U ove datoteke se spremaju preuzeti .pdf rasporedi.
Automatski preuzeti rasporedi se spremaju u datoteku pdf, dok se ručno preuzeti rasporedi spremaju u datoteku pdf2.

Zašto ručno preuzimanje?
Neki automatski preuzeti rasporedi sadrže "dupli" raspored (normalni i za vrijeme školskih praznika) na istoj strani, što onemogućuje ispravno uređivanje sa skriptom editor.py, namijenjene za "jednostruke" rasporede. Rasporedi za linije: 109, 115, 118, 121, 125, 128, 146, 172, 210, 212, 215, 218, 224, 226, 227, 230, 231, 232, 269, 279 i 281 su preuzeti ručno.

## Datoteke txt i txt2
U ove datoteke se spremaju podatci izvađeni iz rasporeda u .pdf formatu.
Za automatski preuzete rasporede se koristi datoteka txt, dok se za ručno preuzete rasporede koristi datoteka txt2.

*Podaci su izvađeni na sljedeći način:
Preuzet je alat "TalkHelper PDF Converter", koji je korišten za grupno prebacivanje .pdf dokumenata u .txt format. Trial ovog alata omogućuje čitanje samo prve stranice pdf-a, ali budući da su rasporedi uvijek na jednoj stranici to je dovoljno.

## Skripta editor.py
Ova skripta čita podatke iz ne-uređenih .txt rasporeda, koji se nalaze u txt i txt2 datotekama. Podatci se formatiraju za lakše čitanje i spremaju u datoteku edited, također u .txt formatu.

## Datoteka edited
Ova datoteka sadrži uređene rasporede u .txt formatu. Trenutno su podaci formatirani za lakšu vizualnu provjeru.

## Skripta display.py
Ova skripta otvara novi prozor i prikazuje podatke iz edited/x.txt (gdje je x broj linije za koju se žele isčitati podatci. X je u kodu definiran varijablom busLineNumber).

## Status projekta
Projekt je trenutno u stanju gdje se samo pokazuje vađenje podataka iz rasporeda.


<img src="/Screenshots/Main.png" alt="Screenshot" width="550" height="400">

