import re

destFolder = "edited/"
srcFolder = "txt/"

# Bus lines with numbers from list were manuali downloaded and placed in pdf2/ folder
manual = [109, 115, 118, 121, 125, 128, 146, 172, 210, 212, 215, 218, 224, 226, 227, 230, 231, 232, 269, 279, 281]

columnSeperatorSize = 40

for i in range(1, 196):
    currentLine = 0

    # For manuali added files change src and dest 
    if (i == 175):
        srcFolder = "txt2/" 

    # Read/Write files
    rf = open(srcFolder + str(i) +".txt", "r", encoding="utf8")
    wf = 0

    try:
        for line in rf:
            currentLine += 1

            # Shrink spaces
            e = re.sub(r"\s\s+", ' ', line)

            if (currentLine == 2 ):
                busLineNumber = re.sub(r"\s+", '', e[13:17])

                try:
                    if ((int(busLineNumber) in manual and i < 175) or 
                        int(busLineNumber) not in range(101, 336)):
                        break
                except ValueError:
                    break

                wf = open(destFolder + busLineNumber + ".txt", "w", encoding="utf-8")
                wf.write(busLineNumber + "\n")

            if (currentLine == 3):
                busLineName = e[14:].split(" - ")
                spaces = "" # Neccessary spaces to fill space between columns

                for j in range(0, columnSeperatorSize-len(busLineName[0])):
                    spaces += " "

                wf.write(busLineName[0] + spaces + busLineName[1].split("NAZIV")[0] + "\n")

            if (currentLine == 5):
                wf.write("\nRadni dan\n")

            if ((currentLine >= 6 and currentLine <= 26) or 
                (currentLine >= 29 and currentLine <= 49) or 
                currentLine >= 52 and currentLine <= 72):

                numbers = e.split(" ")
                numbers = numbers[1:]

                half = int(len(numbers) / 2) # Start index of 2nd column

                if (len(numbers) > 2):
                    try:
                        a = numbers[half-1]
                        b = numbers[half]
                        c = numbers[half+1]
                    except IndexError:
                        break

                    if (len(numbers) == 3):
                        c = c[:len(c)-1]

                    if (b != numbers[0]):
                        half -= int(2 * (0.5 - int(c == numbers[0])))

                editedLine = "";

                for j in range(0, half):
                    if (j == 0):
                        editedLine += numbers[j] + ": "
                    else:
                     editedLine += numbers[j] + " "

                for j in range(0, columnSeperatorSize-len(editedLine)):
                    editedLine += " "

                for j in range(half, len(numbers)-1):
                    if (j == half):
                        editedLine += numbers[j] + ": "
                    else:
                     editedLine += numbers[j] + " "
                editedLine += numbers[len(numbers)-1]

                if (currentLine == 29):
                    wf.write("\nSubota\n")
                if (currentLine == 52):
                    wf.write("\nNedjelja\n")

                wf.write(editedLine)

    except IndexError:
        rf.close()
        if (not (wf == 0)):
            wf.close()
        continue

    rf.close()
    if (not (wf == 0)):
        wf.close()
